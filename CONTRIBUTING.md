Contributors
===

UAIWG
---

| Handle     | Status  |
| ---------- | -------:|
| Forthegy   | Active  |
| Labadore64 | Invited |

Other
---

| Handle | Status |
| ------ | ------:|
| N/A    | N/A    |
