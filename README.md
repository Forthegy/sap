# Sensory Abstraction Project

A collection of partial software and writings on the challenging art of projecting information between sensory spaces, with a focus on translating output for touch interfaces since it is least likely to be fully missing.